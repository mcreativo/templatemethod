<?php


class ClassA extends AbstractClass
{
    protected function doStep1()
    {
        echo 'ClassA doing step one' . PHP_EOL;
    }

    protected function doStep2()
    {
        echo 'ClassA doing step two' . PHP_EOL;
    }

    protected function doStep3()
    {
        echo 'ClassA doing step three' . PHP_EOL;
    }
}