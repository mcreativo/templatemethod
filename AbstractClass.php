<?php


abstract class AbstractClass
{

    /**
     * @param boolean $doStep2
     */
    public function doSomething($doStep2)
    {
        $this->doStep1();
        if ($doStep2)
            $this->doStep2();
        $this->doStep3();
    }

    abstract protected function doStep1();

    abstract protected function doStep2();

    abstract protected function doStep3();
} 