<?php


class ClassB extends ClassA
{
    protected function doStep1()
    {
        echo 'ClassB doing step one' . PHP_EOL;
    }

    protected function doStep2()
    {
        echo 'ClassB doing step two' . PHP_EOL;
    }

    protected function doStep3()
    {
        echo 'ClassB doing step three' . PHP_EOL;
    }
} 